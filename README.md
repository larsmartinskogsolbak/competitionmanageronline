An ASP.NET Core Web Application (MVC) that allows a user to manage information about competitors stored in a database.
The database is managed by Entity Framework. 

The user is able to add new competitors to the database and remove and update records. It is also possible to view all current competitors. 
 